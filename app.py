import json
import requests

from flask import Flask, request
app = Flask(__name__)


def get_product_info(pid):
    url = 'http://product-v3-americanas-npf.internal.b2w.io/product/{pid}'
    response = requests.get(url.format(pid=pid))
    if response.ok:
        return response.json()
    else:
        raise RuntimeError('Erro na obtenção do produto: ' + pid)


def get_offer_info(oid):
    url = 'http://offer-v1-americanas-npf.internal.b2w.io/offer/{oid}'
    response = requests.get(url.format(oid=oid))
    if response.ok:
        return response.json()
    else:
        raise RuntimeError('Erro na obtenção da offer: ' + oid)


def calculate_products_metrics(products):
    metrics = {}
    for product in products:
        pid = product['id']
        product_data = get_product_info(pid)
        p_info = {
            'name': product_data['name'],
            'rating': product_data['rating']
        }

        best_rated = metrics.get('best_rated', {})
        best_rating = best_rated.get('rating', 0)
        if best_rating < product_data['rating']['average']:
            best_rated['pid'] = product_data['id']
            best_rated['rating'] = product_data['rating']['average']
            metrics['best_rated'] = best_rated

        metrics[product_data['id']] = p_info

    return metrics

def calculate_offer_prices(products):
    prices = []
    offers = {}
    for product in products:
        oid_list = product['offers_id_acom']
        for oid in oid_list:
            offer_data = get_offer_info(oid)
            prices.append(offer_data['salesPrice'])

    offers.append ={"Produto:" : product['id'], "Menor oferta:" : prices.min()}

    return offers

@app.route('/product/<pid>')
def product(pid):
    return get_product_info(pid)


@app.route('/products/metrics', methods=['GET', 'POST'])
def products_metrics():
    data = request.get_json()
    return calculate_products_metrics(data['products'])


@app.route('/offer/<oid>')
def offer(oid):
    return get_offer_info(oid)



if __name__ == '__main__':
    app.run(debug=True)
